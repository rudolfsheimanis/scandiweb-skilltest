var gulp = require('gulp');
var sass = require('gulp-sass');
var copy = require('gulp-copy');

gulp.task('styles', function() {
    gulp.src('./node_modules/bulma/bulma.sass')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css/'));

    gulp.src('./sass/**/**')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css/'));
});

gulp.task('scripts', function() {
    gulp.src('./node_modules/vue/dist/vue.min.js')
        .pipe(copy('./js/'));
});

//Watch task
gulp.task('default',function() {
    gulp.watch('./node_modules/bulma/bulma.sass',['styles']);
    gulp.watch('./sass/**/*',['styles']);
});