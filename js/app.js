app = new Vue({
    el: '#app',
    data: {
        selectedOption: '',
        checkboxClass: "checkbox",
        productBlockSize: "is-3",
        products: []
    },
    mounted: function () {
        this.$nextTick(function () {
            axios.get('?api=getProducts')
                .then(function(response)
                {
                    response.data.map(function (item) {
                        app.products.push(item);
                    });

                    console.log(response.data);
                });
        })
    },
    computed: {},
    methods: {
        productColor: function (state) {
            return !state ? 'is-primary' : 'is-danger';
        },
        deleteSelected: function () {

            if (!(this.selectedOption == 'delete')) return false;
            var index = [];

            this.products.map(function (product, key) {
                if (product.checked) {
                    index.push(key);



                }
            });

            index.map(function (key, value) {
                delete(app.products[key]);
            });

            app.products = app.products.filter(function (n) {
                return n != undefined
            });
            app.$forceUpdate();
        },
        getProducts: function () {
            return this.products;
        }
    }
});
