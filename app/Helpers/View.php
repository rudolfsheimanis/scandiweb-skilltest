<?php namespace App\Helpers;

class View
{
    protected $name;
    protected $viewsPath;

    public function __construct($name)
    {
        $this->name = $name;
        $viewsDirectory = System::getConfig('views_directory');
        $this->viewsPath =
            $_SERVER[ 'DOCUMENT_ROOT' ] .
            DIRECTORY_SEPARATOR . 'app' .
            DIRECTORY_SEPARATOR . $viewsDirectory;
    }

    public function asset($asset)
    {
        $rootUrl = (!empty($_SERVER[ 'HTTPS' ]) ? 'https' : 'http') . '://' . $_SERVER[ 'HTTP_HOST' ] . '/';

        return $rootUrl . $asset;
    }

    public function render(Array $variables = [])
    {

        extract($variables);

        ob_start();
        include($this->viewsPath . DIRECTORY_SEPARATOR . $this->name . '.php');
        $view = ob_get_contents();
        ob_end_clean();

        return $view;
    }
}