<?php namespace App\Helpers;

class System
{
    static function getConfig($key)
    {
        $config = parse_ini_file ($_SERVER['DOCUMENT_ROOT'].'/'.'config.ini');
        return isset($config[$key]) ? $config[$key] : null;
    }
}