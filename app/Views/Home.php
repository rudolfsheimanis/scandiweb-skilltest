<html>
<head>
    <meta charset="utf-8">
    <title>ScandiTest Homepage</title>
    <script src="<?= $this->asset('node_modules/vue/dist/vue.js') ?>"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="<?= $this->asset('css/bulma.css') ?>">
    <link rel="stylesheet" href="<?= $this->asset('css/app.css') ?>">
</head>
<body>
<div id="app" class="container">
    <header>

        <nav class="navbar">
            <div class="navbar-brand">
                <a class="navbar-item" href="/">
                    <img src="<?= $this->asset('img/logo.png') ?>"/>
                </a>
                <div class="navbar-burger" data-target="nav-menu">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
            </div>
            <div id="nav-menu" class="navbar-menu">
                <div class="navbar-start">
                    <a class="navbar-item" href="/">
                        Products list
                    </a>
                    <a class="navbar-item" href="/">
                        Products add
                    </a>
                </div>

                <div class="navbar-end">
                    <div class="navbar-item is-hidden-desktop-only" href="https://twitter.com/jgthms" target="_blank">
                        <div class="field">
                            <div class="control">
                                <div class="select">
                                    <select v-model="selectedOption">
                                        <option selected>Choose an action</option>
                                        <option value="delete">Delete selected</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="navbar-item is-hidden-desktop-only" href="https://twitter.com/jgthms" target="_blank">
                        <div class="field">
                            <div class="control">
                                <button @click="deleteSelected()" class="button is-primary">Apply</button>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </nav>
    </header>
    <section class="section">
        <div v-if="products.length" class="columns is-multiline">
            <div  v-for="(product,index) in products" class="column" v-bind:class="productBlockSize">
                <div class="tile is-parent ">
                    <article class="tile is-child notification" v-bind:class="productColor(product.checked)">
                        <p class="subtitle">[{{ product.category }}]
                            <span class="is-pulled-right">
                                    <input v-bind:class="checkboxClass"
                                           :name="product.name" v-model="product.checked" type="checkbox"/>
                                </span>
                        </p>


                        <p class="title">{{ product.name }}</p>
                        <p class="subtitle">Price: ${{ product.price }}</p>
                        <article v-if="product.image" class="tile is-child box">
                            <figure class="image is-2by1">
                                <img :src="product.image"/>
                            </figure>
                        </article>
                        <p class="is-size-5">
                           {{product.description}}
                        </p>


                    </article>
                </div>
            </div>
        </div>
        <div v-else>
            <h1>Out of products, sorry.</h1>
        </div>
    </section>
</div>
</body>
<script src="<?= $this->asset('js/axios.min.js') ?>"></script>
<script src="<?= $this->asset('js/app.js') ?>"></script>
<script src="<?= $this->asset('js/nav.js') ?>" type="text/javascript"></script>
</html>