<?php namespace App\Database;

class DataBase  
{

	private $database;
	private $databasePath;

	public function __construct( $db = 'mainDB' )
	{
		$this->database = $db;
	}

	private function connect()
	{
		$dbData = $this->getDatabasePath($this->database);
		try
		{
			$pdo = new \PDO($dbData);
		}
		catch(\PDOException $e)
		{
			return $e->getMessage();
		}
	}

	private function getDatabasePath($dbName)
	{
		$this->databasePath = 'sqlite:/'.__DIR__.DIRECTORY_SEPARATOR.$dbName.'.sqlite';
		var_dump($this->databasePath);
	}

	public function executeSql($sql)
	{
		$pdo = $this->connect();

		if( !($pdo instanceof \PDO) )
		{
			return $pdo;
		}

		try
		{
			$pdo->exec($sql);
		}
		catch(\PDOException $e)
		{
			return $e->getMessage();
		}

		return true;
	}

	public function dbDelete($table, $where)
	{
		return $this->executeSql("DELETE FROM {$table} WHERE {$where}");
	}

	public function dbInsert($table, $values)
	{
		$columns = [];
		$columnValues = [];

		foreach($values as $column => $value)
		{
			$columns[] = $column;
			$columnValues[] = $value;
		}

		$columns = implode(",", $columns);
		$columnValues = implode(",", $columnValues);

		if( empty($columns) || empty($columnValues) )
		{
			return false;
		}

		return $this->executeSql("INSERT INTO {$table} ({$columns}) VALUES ({$columnValues})");
	}



}