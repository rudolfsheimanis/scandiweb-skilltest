<?php namespace App\Controllers;

use App\Helpers\View;
use App\Models\Product;

class PageController
{
    protected $getVars;
    protected $postVars;

    protected $routes =
        [
            'home',
        ];

    public function __construct($getVars, $postVars)
    {
        $this->getVars = $getVars;
        $this->postVars = $postVars;
    }

    public function handle()
    {

        if (isset($this->getVars[ 'api' ]))
        {
            return $this->getProductsApi($this->getVars[ 'api' ]);
        }
        if ( !isset($this->getVars[ 'page' ]) || empty($this->getVars[ 'page' ]))
        {
            $route = 'home';
        }
        else
        {
            $route = $this->getRouteIfExists();
        }

        if ($route)
        {
            return $this->{'get' . ucfirst($route)}();
        }

        return http_response_code(404);
    }

    protected function getRouteIfExists($route = null)
    {
        if (is_null($route))
        {
            $route = isset($this->getVars[ 'page' ]) ? $this->getVars[ 'page' ] : false;
        }

        if ( !$route)
        {
            return false;
        }

        $route = strtolower($route);

        if (in_array($route, $this->routes))
        {
            return $route;
        }

        return false;
    }

    public function getHome()
    {
        $products = Product::all();
        $view = new View('Home');
        die($view->render([
            'products' => $products,
        ]));
    }

    public function getProductsApi()
    {
        $products = Product::all();
        $productsRefactored = [];

        foreach( $products as $product )
        {
            $productsRefactored[] =
                [
                    'id' => $product->getId(),
                    'name' => $product->getName(),
                    'price' => $product->getPrice(),
                    'description' => $product->getDescription(),
                    'category' => 'imported from api',
                    'image' => 'https://scandiweb.com/assets/images/services/design/mobile.png',
                    'checked' => false,
                ];
        }
        die(json_encode($productsRefactored));
    }

}
