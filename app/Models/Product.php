<?php namespace App\Models;

use App\Database\Orm;

class Product extends Orm
{

    protected static $table = 'products';

  	protected
  		$name,
  		$price,
  		$data,
  		$add_date;

  	public function getColumnNames()
  	{
  		return ['name','price','data'];
  	}

  	public function get($fieldName = false)
  	{
  		return 
  		[
  		'name' => $this->name,
  		'price' => $this->price,
  		'data' => $this->data,
  		];
  	}

	public function getTitleAndMessage()
	{
		return $this->name.' '.$this->price;
	}

	public function setParams($s)
	{
		$this->name = $s;
		$this->data = $s;
		$this->price = $s;
		//$this->save();
	}

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @return string
     */
    public static function getId()
    {
        return self::$pk;
    }



    public function getDescription()
    {
        return 'This has not been implemented yet, but it will be soon. For now stick with this.';
    }
}
