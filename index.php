<?php

use App\Helpers\System;
use App\Controllers\PageController;
use App\Database\Orm;

require_once __DIR__ . '/vendor/autoload.php';

$conn = new mysqli(
    System::getConfig('db_host'),
    System::getConfig('db_user'),
    System::getConfig('db_password'));

Orm::useConnection($conn, 'scanditest');

$requestHandler = new PageController($_GET, $_POST);
return $requestHandler->handle();