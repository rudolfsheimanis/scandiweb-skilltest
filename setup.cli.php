<?php
(PHP_SAPI !== 'cli' || isset($_SERVER['HTTP_USER_AGENT'])) && die('cli only');
echo '-- Application setup started --'.PHP_EOL;
try
{
	$dbConfig = parse_ini_file (__DIR__ . '/');
	echo '- Configuration fetched -'.PHP_EOL;
}
catch(Exception $e)
{
	echo '- Unable to parse configuration -'.PHP_EOL;
	die($e);
}

try
{
	$conn = new mysqli($dbConfig['db_host'],$dbConfig['db_user'],$dbConfig['db_password']);
}
catch(Exception $e)
{
	echo '- Unable to connect to the DB using credentials in config -'.PHP_EOL;
	die($conn->error);
}

try
{
	echo '- Trying to make a database -'.PHP_EOL;
	$conn->query('CREATE DATABASE '.$dbConfig['db_database']);
	echo '- Database successfully created -'.PHP_EOL;
	$conn = new mysqli($dbConfig['db_host'],$dbConfig['db_user'],$dbConfig['db_password'],$dbConfig['db_database']);
}
catch(Exception $e)
{
	echo '- ERROR: Couldnt create database.'.PHP_EOL;
	echo sprintf($conn->error).PHP_EOL;
}

$dbTables = [];

$dbTables[] = "CREATE TABLE products (
id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY, 
name VARCHAR(125) NOT NULL,
price REAL(20,2) NOT NULL,
data TEXT NOT NULL,
add_date TIMESTAMP
)";

echo '- Setting up tables. -'.PHP_EOL;
foreach( $dbTables as $tableSql)
{
	try
	{
		if($conn->query($tableSql) !== TRUE)
		{
			throw new Exception($conn->error);
		}
	}
	catch(Exception $e)
	{
		echo '- '.sprintf($e->getMessage()).' -'.PHP_EOL;
	}
}
echo '-- Setup finished --'.PHP_EOL;